﻿using Unity.Mathematics;
using UnityEngine;

namespace Monoid.Unity.Mesh.Shapes.Demo {

  using Mesh = UnityEngine.Mesh;

  [RequireComponent (typeof (MeshFilter), typeof (MeshRenderer))]
  public class SphereShape : MonoBehaviour {
    public float radius = 1;
    public int2 slices = math.int2 (8, 8);

    void Awake () {
      Refresh ();
    }

    void Refresh () {
      var meshFilter = GetComponent<MeshFilter> ();
      var mesh = meshFilter.sharedMesh;
      if (!mesh) {
        meshFilter.sharedMesh = mesh = new Mesh ();
      }
      slices = math.max (0, slices);
      mesh.Set (Create.Sphere (radius, slices.x, slices.y));
    }

    void OnValidate () {
      Refresh ();
    }
  }

}
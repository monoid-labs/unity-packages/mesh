﻿using Unity.Mathematics;
using UnityEngine;

namespace Monoid.Unity.Mesh.Shapes.Demo {

  using Mesh = UnityEngine.Mesh;

  [RequireComponent (typeof (MeshFilter), typeof (MeshRenderer))]
  public class ArcShape : MonoBehaviour {
    public float2 radius = math.float2 (0, 1);
    public float2 angle = math.float2 ((float) - math.PI, (float) math.PI);
    public int2 slices = math.int2 (8, 0);

    void Awake () {
      Refresh ();
    }

    void Refresh () {
      var meshFilter = GetComponent<MeshFilter> ();
      var mesh = meshFilter.sharedMesh;
      if (!mesh) {
        meshFilter.sharedMesh = mesh = new Mesh ();
      }
      slices = math.max (0, slices);
      mesh.Set (Create.DiscSector (radius.x, radius.y, math.abs (angle.y - angle.x), 0.5f * (angle.x + angle.y), slices.x, slices.y));
    }

    void OnValidate () {
      Refresh ();
    }
  }

}
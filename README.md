Common Geometric Shapes for Unity3D
===================================

Srcipts to generate common geometric shapes to be used as meshes.


License
-------

MIT License (see [LICENSE](LICENSE.md))

using Unity.Mathematics;

namespace Monoid.Unity.Mesh.Shapes {
  public static class Create {

    public static TransformedShape<T> Transform<T> (T shape, float4x4 transform) where T : IShape {
      return new TransformedShape<T> (shape, transform);
    }

    public static Rect Rect (float x, float y, int xSlices = 0, int ySlices = 0) {
      return new Rect { extent = 0.5f * math.float2 (x, y), slices = math.int2 (xSlices, ySlices) };
    }

    public static Arc Circle (float radius, int segmentSlices = 32, int loopSlices = 0) {
      return Disc (0.0f, radius, segmentSlices, loopSlices);
    }

    public static Arc CircleSector (float radius, float angle, float rotation = 0.0f, int segmentSlices = 32, int loopSlices = 0) {
      return DiscSector (0.0f, radius, angle, rotation, segmentSlices, loopSlices);
    }

    public static Arc Disc (float innerRadius, float outerRadius, int segmentSlices = 32, int loopSlices = 0) {
      return DiscSector (innerRadius, outerRadius, 2.0f * (float) math.PI, 0.0f, segmentSlices, loopSlices);
    }

    public static Arc DiscSector (float innerRadius, float outerRadius, float angle, float rotation = 0.0f, int segmentSlices = 32, int loopSlices = 0) {
      return new Arc { radius = math.float2 (innerRadius, outerRadius), angle = math.float2 (rotation - 0.5f * angle, rotation + 0.5f * angle), slices = math.int2 (segmentSlices, loopSlices) };
    }

    public static Sphere Sphere (float radius, int segmentSlices = 32, int loopSlices = 32) {
      return new Sphere { radius = radius, slices = math.int2 (segmentSlices, loopSlices) };
    }

  }
}
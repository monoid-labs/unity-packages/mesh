﻿using Unity.Mathematics;

namespace Monoid.Unity.Mesh.Shapes {
  public struct Sphere : IShape {

    public float radius;
    public int2 slices;

    public int Vertices => (slices.x + 2) * (slices.y + 2);
    public int Quads => (slices.x + 1) * (slices.y + 1);
    public int Triangles => 2 * (slices.x + 1) * (slices.y + 1);

    public int4 Quad (int index) {
      var n = slices.x + 1;
      var i = math.int4 (index % n, index / n, 0, 1);
      return (n + 1) * (i.yyyy + i.zwzw) + (i.xxxx + i.zzww);
    }

    public int3 Triangle (int index) {
      var quad = Quad (index / 2);
      return index % 2 == 0 ? quad.xyz : quad.wzy;
    }

    public float3 Position (int index) => Normal (index) * radius;

    public float3 Normal (int index) {
      var uv = TexCoord (index);
      uv.y = 1.0f - uv.y;
      uv.x *= -2.0f;
      uv *= (float) math.PI;
      math.sincos (uv, out var s, out var c);
      return math.float3 (s.yy * math.float2 (c.x, s.x), c.y);
    }

    public float2 TexCoord (int index) {
      var n = slices.x + 2;
      var i = math.float2 (index % n, index / n);
      return i / (slices + 1);
    }
  }
}
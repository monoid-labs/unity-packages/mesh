using Unity.Mathematics;

namespace Monoid.Unity.Mesh.Shapes {

  public interface IShape {
    int Vertices { get; }
    int Triangles { get; }

    int3 Triangle (int index);

    float3 Position (int index);
    float3 Normal (int index);
    float2 TexCoord (int index);
  }

}
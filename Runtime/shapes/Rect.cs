using Unity.Mathematics;

namespace Monoid.Unity.Mesh.Shapes {

  public struct Rect : IShape {
    public float2 extent;
    public int2 slices;

    public int Vertices => (slices.x + 2) * (slices.y + 2);
    public int Quads => (slices.x + 1) * (slices.y + 1);

    public int Triangles => 2 * (slices.x + 1) * (slices.y + 1);

    public int4 Quad (int index) {
      var n = slices.x + 1;
      var i = math.int4 (index % n, index / n, 0, 1);
      return (n + 1) * (i.yyyy + i.zwzw) + (i.xxxx + i.zzww);
    }

    public int3 Triangle (int index) {
      var quad = Quad (index / 2);
      return index % 2 == 0 ? quad.xyz : quad.wzy;
    }

    public float3 Position (int index) => math.float3 (-extent.xy + 2.0f * extent.xy * Coord (index), 0);

    public float3 Normal (int index) {
      return math.float3 (0, 0, 1);
    }

    public float2 TexCoord (int index) => Coord (index);

    private float2 Coord (int index) {
      var n = slices.x + 2;
      var i = math.float2 (index % n, index / n);
      return i / (slices + 1);
    }

  }

}